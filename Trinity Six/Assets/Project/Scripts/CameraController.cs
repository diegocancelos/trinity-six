﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController : MonoBehaviour
{
    public Transform target;
    
    public Vector3 offset;

    public bool useOffsetValues;

    public float rotateSpeed;

    public Transform pivot;

    public float maxViewAngle;
    public float minViewAngle;

    public bool invertY;

    
    void Start()
    {
        if(!useOffsetValues){
            offset = target.position - transform.position;
        }

        //Ubicamos el pivot a la posicion del jugador
        pivot.transform.position = target.transform.position;
        pivot.transform.parent = target.transform;

        //Escondemos el cursor al empezar la partida
        Cursor.lockState = CursorLockMode.Locked;
    }

    void LateUpdate()
    {
        //Obtenemos la posicion x del mouse rotamos el personaje
        float horizontal = Input.GetAxis("Mouse X") * rotateSpeed;
        target.Rotate(0, horizontal, 0);

        //Obtenemos la posicion y del mouse y rotamos el pivot
        float vertical = Input.GetAxis("Mouse Y") * rotateSpeed;
        if(invertY){
            pivot.Rotate(vertical, 0, 0);
        } else{
            pivot.Rotate(-vertical, 0, 0);
        }

        //Limitamos la subida y bajada de la camera 
        if(pivot.rotation.eulerAngles.x > maxViewAngle && pivot.rotation.eulerAngles.x < 180f){
            pivot.rotation = Quaternion.Euler(maxViewAngle, 0, 0);
        } 

        if(pivot.rotation.eulerAngles.x > 180f && pivot.rotation.eulerAngles.x < 360f + minViewAngle){
            pivot.rotation = Quaternion.Euler(360f + minViewAngle, 0, 0);
        }

        //Movemos la camara basandonos en la posicion del jugador y el offset 
        float desiredYAngle = target.eulerAngles.y;
        float desiredXAngle = pivot.eulerAngles.x;

        Quaternion rotation = Quaternion.Euler(desiredXAngle, desiredYAngle, 0);
        transform.position = target.position - (rotation * offset);

        //Impedimos que la camara pueda ver el subsuelo
        if(transform.position.y < target.position.y){
            transform.position = new Vector3(transform.position.x, target.position.y -.5f, transform.position.z);
        }

        transform.LookAt(target);
    }
}
