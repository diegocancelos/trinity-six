﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    private float horizontalMove;
    private float verticalMove;
    private Vector3 playerInput;

    public CharacterController player;
    public float playerSpeed;
    private Vector3 movePlayer;

    public Camera mainCamera;
    private Vector3 camForward;
    private Vector3 camRight; 

    public float gravityScale;
    public float jumpForce;

    // Start is called before the first frame update
    void Start()
    {
        player = GetComponent<CharacterController>();
    }

    // Update is called once per frame
    void Update()
    {

        //Movimiento y direcciones
        float yStore = playerInput.y;
        playerInput = (transform.forward * Input.GetAxis("Vertical")) + (transform.right * Input.GetAxis("Horizontal"));
        playerInput = playerInput.normalized * playerSpeed;
        playerInput.y = yStore;

        //Velocidad por frame
        player.Move(playerInput * Time.deltaTime);

        //Salto
        if(player.isGrounded){
            playerInput.y = 0f;
            if(Input.GetButtonDown("Jump")){
                playerInput.y = jumpForce;
            }
        }

        //Caida
        playerInput.y = playerInput.y + (Physics.gravity.y * gravityScale * Time.deltaTime);

        /*horizontalMove = Input.GetAxis("Horizontal");
        verticalMove = Input.GetAxis("Vertical");

        //Valores de movimiento y arreglo a velocidad diagonal
        playerInput = new Vector3(horizontalMove, 0, verticalMove);
        playerInput = Vector3.ClampMagnitude(playerInput, 1); 

        camDirection();

        //Ajustamos la camara al movimiento
        movePlayer = playerInput.x * camRight + playerInput.z * camForward; 

        //Giro del personaje
        player.transform.LookAt(player.transform.position + movePlayer);

        //Movimiento
        //player.Move(movePlayer * playerSpeed * Time.deltaTime); */

    }

    /*
    void camDirection(){
        camForward = mainCamera.transform.forward;
        camRight = mainCamera.transform.right;

        //Anulamos el eje y
        camForward.y = 0;
        camRight.y = 0;

        //Valores reales de las direcciones
        camForward = camForward.normalized;
        camRight = camRight.normalized;
    }*/

}
