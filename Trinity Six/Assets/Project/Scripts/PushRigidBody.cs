﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PushRigidBody : MonoBehaviour
{
    public float pushPower = 2.0f;

    private float targetMass;

    private void OnControllerColliderHit(ControllerColliderHit hit)
    {
        Rigidbody body = hit.collider.attachedRigidbody;

        //Comprobamos si tiene rigidbody el componente
        if(body == null || body.isKinematic){
            return;
        }

        //Comprobamos que no lo estamos tocando por encima
        if (hit.moveDirection.y < -0.3){
            return;
        }

        //Almacenamos la masa del objeto
        targetMass = body.mass;

        //Almacenamos la direccion en la que estamos empujando el objeto
        Vector3 pushDir = new Vector3(hit.moveDirection.x, 0, hit.moveDirection.z);

        //Movemos el objeto teniendo en cuenta su masa
        body.velocity = pushDir * pushPower / targetMass;
    }
}
