﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
public class PauseMenu : MonoBehaviour
{

    public static bool GameIsPaused = false;

    public GameObject pauseMenuUI;

    public GameObject pauseConfigUI;
    
    private string escena;

    void Start()
    {
        pauseMenuUI.SetActive(false);
        pauseConfigUI.SetActive(false);
        Resume();
        Screen.lockCursor = false; 
    }

    // Update is called once per frame
    void Update()
    {
        if(Input.GetKeyDown(KeyCode.Escape))
        {
            Screen.lockCursor = false;
            if(GameIsPaused)
            {
                if(pauseConfigUI == false)
                {
                    Resume();
                }
            }else
            {
                Pause();
            }
        }
    }

    public void Resume()
    {
        pauseMenuUI.SetActive(false);
        Time.timeScale = 1f;
        GameIsPaused = false;
        PlayerPrefs.SetInt("GameIsPaused", 0);
        Screen.lockCursor = true; 
    }

    void Pause()
    {
        pauseMenuUI.SetActive(true);
        Time.timeScale = 0f;
        GameIsPaused = true;
        PlayerPrefs.SetInt("GameIsPaused", 1);
    }

    public void QuitGame()
    {
        escena = "1-LauncherScene";
        Time.timeScale = 1f;
        SceneManager.LoadScene(escena);
    }
}
