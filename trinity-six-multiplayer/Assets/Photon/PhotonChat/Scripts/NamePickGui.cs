﻿using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof (PhotonChatManager))]
public class NamePickGui : MonoBehaviour
{
    public PhotonChatManager chatNewComponent;
    const string playerNamePrefKey = "PlayerName";

    public void Start()
    {
        this.chatNewComponent = FindObjectOfType<PhotonChatManager>();
        this.StartChat();
        Debug.Log("Chat Instance Initialized");
    }

    public void Update()
    {
        //if (Input.GetKey(KeyCode.Return) || Input.GetKey(KeyCode.KeypadEnter))
        //{
        //    this.StartChat();
        //    Debug.Log("starting Chat");
        //}
    }

    public void StartChat()
    {
        PhotonChatManager chatNewComponent = FindObjectOfType<PhotonChatManager>();
        chatNewComponent.UserName = PlayerPrefs.GetString(playerNamePrefKey);
        chatNewComponent.Connect();
        enabled = false;

        //PlayerPrefs.SetString(NamePickGui.UserNamePlayerPref, chatNewComponent.UserName);
    }
}