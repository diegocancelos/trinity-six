﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PickUpObjects : MonoBehaviour
{
    public GameObject ObjectToPickUp;
    public GameObject PickedObject;
    public Transform interactionZone;
    
    void Update()
    {
        //Si hay algo en la zona de deteccion, se pueda coger y no tengamos ningun objeto ya cogido
        if(ObjectToPickUp != null && ObjectToPickUp.GetComponent<PickableObject>().isPickable == true && PickedObject == null){
            if(Input.GetKeyDown(KeyCode.F)){
                PickedObject = ObjectToPickUp;
                PickedObject.GetComponent<PickableObject>().isPickable = false;
                PickedObject.transform.SetParent(interactionZone);
                PickedObject.transform.position = interactionZone.position;
                PickedObject.GetComponent<Rigidbody>().useGravity = false;
                PickedObject.GetComponent<Rigidbody>().isKinematic = true;
            }
        }
        //Si tenemos un objeto en la mano
        else if(PickedObject != null){
            //Si queremos dejarlo en el suelo
            if(Input.GetKeyDown(KeyCode.F)){
                PickedObject.GetComponent<PickableObject>().isPickable = true;
                PickedObject.transform.SetParent(null);
                PickedObject.GetComponent<Rigidbody>().useGravity = true;
                PickedObject.GetComponent<Rigidbody>().isKinematic = false;
                PickedObject = null;
            }
            //Si queremos lanzar el objeto
            if(Input.GetMouseButtonUp (0)){
                PickedObject.transform.SetParent(null);
                PickedObject.GetComponent<Rigidbody>().AddForce(transform.forward * 2000 * 3, ForceMode.Impulse);
                PickedObject.GetComponent<PickableObject>().isPickable = true;
                PickedObject.GetComponent<Rigidbody>().useGravity = true;
                PickedObject.GetComponent<Rigidbody>().isKinematic = false;
                PickedObject = null;
            }
        }
    }
}
