﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GrapplingHook : MonoBehaviour
{
    public GameObject hook;
    public GameObject hookHolder;

    public float hookTravelSpeed;
    public float playerTravelSpeed;

    public static bool fired;
    public bool hooked;
    public GameObject hookedObj;

    public float maxDistance;
    private float currentDistance;

    //Climb up the object
    private bool grounded;

    void Update()
    {
        //Lanzamiento del gancho
        if(Input.GetMouseButtonDown(0) && fired == false){
            fired = true;
        }

        //Si hemos lanzado el gancho y no estamos enganchados
        if (fired == true && hooked == false){
            hook.transform.Translate(Vector3.forward * Time.deltaTime * hookTravelSpeed);
            currentDistance = Vector3.Distance(transform.position, hook.transform.position);

            if (currentDistance >= maxDistance){
                ReturnHook();
            }
        }

        if(hooked == true && fired == true){
            hook.transform.parent = hookedObj.transform;

            transform.position = Vector3.MoveTowards(transform.position, hook.transform.position, Time.deltaTime * playerTravelSpeed);
            float distanceToHook = Vector3.Distance(transform.position, hook.transform.position);

            this.GetComponent<Rigidbody>().useGravity = false;

            if(distanceToHook < 1){
                if(grounded == false){
                    this.transform.Translate(Vector3.forward * Time.deltaTime * 25f);
                    this.transform.Translate(Vector3.up * Time.deltaTime * 50f);
                }

                StartCoroutine("Climb");
            }
        } 
        else{
            hook.transform.parent = hookHolder.transform;
            this.GetComponent<Rigidbody>().useGravity = true;
        }
    }

    IEnumerator Climb()
    {
        yield return new WaitForSeconds(0.1f);
        ReturnHook();
    }

    void ReturnHook()
    {
        hook.transform.rotation = hookHolder.transform.rotation;
        hook.transform.position = hookHolder.transform.position;
        fired = false;
        hooked = false;
    }

    void CheckIfGrounded()
    {
        RaycastHit hit;
        float distance = 1f;

        Vector3 dir = new Vector3(0,-1);

        if(Physics.Raycast(transform.position, dir, out hit, distance)){
            grounded = true;
        }
        else{
            grounded = false;
        }
    }
}
