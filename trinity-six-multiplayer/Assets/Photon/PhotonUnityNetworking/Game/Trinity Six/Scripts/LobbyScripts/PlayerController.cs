﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour {

	public float mouseSensitivityX = 1.0f;
	public float mouseSensitivityY = 1.0f;

	public float walkSpeed = 10.0f;
	Vector3 moveAmount;
	Vector3 smoothMoveVelocity;

	Transform cameraT;
	float verticalLookRotation;

	Rigidbody rigidbodyR;

	public float jumpForce = 250.0f;
	bool grounded;
	public LayerMask groundedMask;

	//bool cursorVisible;

    //Correr por las paredes
    public float wallRunSpeed = 15f;
    public float wallRunTime = 3f;
    float originalTime;

    bool wallRun;

	private int GameIsPaused;
	private int chatActive;

	// Use this for initialization
	void Start () {
		cameraT = Camera.main.transform;
		rigidbodyR = GetComponent<Rigidbody> ();
		//LockMouse ();

        originalTime = wallRunTime;
	}
	
	// Update is called once per frame
	void Update () {
		GameIsPaused = PlayerPrefs.GetInt("GameIsPaused");
		chatActive = PlayerPrefs.GetInt("chatActive");
		if(GameIsPaused == 0 && chatActive == 0)
		{
			//Rotacion
			transform.Rotate (Vector3.up * Input.GetAxis ("Mouse X") * mouseSensitivityX);
			verticalLookRotation += Input.GetAxis ("Mouse Y") * mouseSensitivityY;
			verticalLookRotation = Mathf.Clamp (verticalLookRotation, -60, 60);
			cameraT.localEulerAngles = Vector3.left * verticalLookRotation;

			//Movimiento
			Vector3 moveDir = new Vector3 (Input.GetAxisRaw ("Horizontal"), 0, Input.GetAxisRaw ("Vertical")).normalized;
			Vector3 targetMoveAmount = moveDir * walkSpeed;
			moveAmount = Vector3.SmoothDamp (moveAmount, targetMoveAmount, ref smoothMoveVelocity, .15f);

			//Salto
			if (Input.GetButtonDown ("Jump")) {
				if (grounded) {
					rigidbodyR.AddForce (transform.up * jumpForce);
				}
			}

			Ray ray = new Ray (transform.position, -transform.up);
			RaycastHit hit;

			if (Physics.Raycast(ray, out hit, 1 + .1f, groundedMask)) {
				grounded = true;
			}
			else {
				grounded = false;
			}

			/* Bloquea y desbloquea el raton al hacer click*/
			// if (Input.GetMouseButtonUp (0)) {
			// 	if (!cursorVisible) {
			// 		UnlockMouse ();
			// 	} else {
			// 		LockMouse ();
			// 	}
			// }
		}
		
	}

	void FixedUpdate() {
        if(!wallRun){
            if(!rigidbodyR.useGravity){
                rigidbodyR.useGravity = true;
            }
            rigidbodyR.MovePosition (rigidbodyR.position + transform.TransformDirection (moveAmount) * Time.fixedDeltaTime);
        }
        else{
            WallRun();
        }

        if(wallRun){
            wallRunTime -= Time.fixedDeltaTime;

            if(wallRunTime <= 0){
                wallRunTime = originalTime;
                wallRun = false;
            }
        }

		
	}

	// void UnlockMouse() {
	// 	Cursor.lockState = CursorLockMode.None;
	// 	Cursor.visible = true;
	// 	cursorVisible = true;
	// }

	// void LockMouse() {
	// 	Cursor.lockState = CursorLockMode.Locked;
	// 	Cursor.visible = false;
	// 	cursorVisible = false;
	// }

    void WallRun()
    {
        if(rigidbodyR.useGravity){
            rigidbodyR.useGravity = false;
        }

        rigidbodyR.AddForce(transform.forward * wallRunSpeed, ForceMode.Acceleration);
		rigidbodyR.AddForce(transform.up * (wallRunSpeed/2));
    }

    private void OnCollisionEnter(Collision collision)
    {
        if(collision.gameObject.tag == "Ground"){
            grounded = true;
        }
        if(!grounded && collision.gameObject.tag == "Runnable Wall"){
            wallRun = true;
        }
    }

    private void OnCollisionExit(Collision collision)
    {
        if(collision.gameObject.tag == "Ground"){
            grounded = false;
        }
        else if(collision.gameObject.tag == "Runnable Wall"){
            wallRunTime = originalTime;
            wallRun = false;
        }
    }
}