﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AnimBoto : MonoBehaviour, IAction
{

    [SerializeField]
    private Color activeColor;
    [SerializeField]
    private Color inactiveColor;
    [SerializeField]
    private Material material;

    private bool activated;
    

    // Start is called before the first frame update
    void Start()
    {
        material.color = inactiveColor;
    }

    // Update is called once per frame
    public void Activate()
    {
        activated = !activated;
        if(activated){
            material.color = activeColor;
        }
        else{
            material.color = inactiveColor;
        }
    }
}
