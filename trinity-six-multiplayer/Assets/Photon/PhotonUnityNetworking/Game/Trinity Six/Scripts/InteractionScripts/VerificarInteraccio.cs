﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VerificarInteraccio : MonoBehaviour
{

    [SerializeField]
    private float minInteractionDistance;
    [SerializeField]
    private GameObject rayOrigin;
    private Ray ray;
    private bool canInteract;
    private ButtonObject currentReceiver;

    private void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        CheckRayCast();
        if(canInteract){
            if(Input.GetKeyDown(KeyCode.F)){
                currentReceiver.Activate();
            }
        }
    }

    private void CheckRayCast(){
        ray = new Ray(rayOrigin.transform.position, rayOrigin.transform.forward);
        RaycastHit hit;
        if(Physics.Raycast(ray, out hit)){
            if(hit.distance < minInteractionDistance){
                currentReceiver = hit.transform.gameObject.GetComponent<ButtonObject>();
                if(currentReceiver != null){
                    Debug.Log(currentReceiver.GetInteractionMessage());
                    canInteract = true;
                }
                else{
                    canInteract = false;
                }
            }
        }
    }
}
