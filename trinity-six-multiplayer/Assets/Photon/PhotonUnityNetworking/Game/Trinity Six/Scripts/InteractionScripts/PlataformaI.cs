using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlataformaI : MonoBehaviour, IAction
{
    [SerializeField]
    private Transform startPoint;
    [SerializeField]
    private Transform endPoint;
    [SerializeField]
    private float speed;
    private bool activated;
    private Vector3 moveDirection;

    void Start()
    {
        moveDirection = Vector3.Normalize(endPoint.position - startPoint.position);
    }

    void FixedUpdate()
    {
        if(activated){
            if(Vector3.Distance(gameObject.transform.position, endPoint.position) > 0.05f){
                gameObject.transform.position += moveDirection * speed/100;
            }
        }
        else{
            if(Vector3.Distance(gameObject.transform.position, startPoint.position) > 0.05f){
                gameObject.transform.position -= moveDirection * speed/100;
            }
        }
    }

    public void Activate()
    {
        activated = !activated;
        if(activated){
            Debug.Log("Plataforma visible!");
        }
        else{
            Debug.Log("Plataforma amagada!");
        }
    }
       
}