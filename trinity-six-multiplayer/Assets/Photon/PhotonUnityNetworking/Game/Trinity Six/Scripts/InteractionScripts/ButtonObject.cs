using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Collider))]

public class ButtonObject : MonoBehaviour 
{

    [SerializeField]
    private string interactMessage;

    [SerializeField]
    private GameObject[] objectesInteraccio;

    public void Activate(){
        foreach (GameObject obj in objectesInteraccio)
        {
            obj.GetComponent<IAction>().Activate();
        }
    }

    public string GetInteractionMessage()
    {
        return interactMessage;
    }

}